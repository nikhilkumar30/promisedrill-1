// Problem2:
// Create a function named composePromises.
// This function should take an array of Promises as input and return a new Promise.
// The new Promise should resolve with an array containing the results of all the input Promises, in the order they were provided.

const promise1 = new Promise((resolve) => {
  resolve("promise1 is successful :) ");
});

const promise2 = new Promise((resolve) => {
  resolve("promise2 is successful :) ");
});

const promise3 = new Promise((resolve) => {
  resolve("promise3 is successful :) ");
});

function composePromises() {
  return Promise.all([promise1, promise2, promise3]);
}

composePromises().then((result) => {
  console.log(result);
});
