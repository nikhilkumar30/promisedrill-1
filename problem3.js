// Problem3:
// Create a function named dynamicChain.
// This function should take an array of functions that return Promises as input.
// Use a loop to dynamically chain the Promises returned by each function in the array.
// The final Promise should resolve with the result of the last function in the array.


function promise1() {
    return new Promise((resolve) => {
        setTimeout(()=>{
            resolve("success promise1");
        });
    })
}
function promise2() {
    return new Promise((resolve) => {
        setTimeout(()=>{
            resolve("success promise2");
        });
    })
}
function promise3() {
    return new Promise((resolve) => {
        setTimeout(()=>{
            resolve("success promise3");
        });
    })
}

const arrayFunctions=[promise1(),promise2(),promise3()];

function dynamicChain(arrayFunctions) {
    let result;
    arrayFunctions.forEach(element => {
        result=element;
    });
    return result;
}

dynamicChain(arrayFunctions).then((result)=>{
    console.log(result);
})
.catch((err)=>{
    console.error(err);
})






























// const promise1 = new Promise((resolve) => {
//   setTimeout(() => {
//     resolve("promise1 is successful :) ");
//   },3000);
// });

// const promise2 = new Promise((resolve) => {
//   setTimeout(() => {
//     resolve("promise2 is successful :) ");
//   },1500);
// });

// const promise3 = new Promise((resolve) => {
//   setTimeout(() => {
//     resolve("promise3 is successful :) ");
//   },2000);
// });

// const arr = [promise1, promise2, promise3];

// function dynamicChain(functions) {
//   return functions.reduce((chain, func) =>
//     chain.then(func), Promise.resolve())
// }

// dynamicChain(arr)
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((err) => {
//     console.error(err);
//   });
