// Problem1:
// Create two functions, racePromise1 and racePromise2.
// Both functions should return a Promise that resolves with a unique success message after a random delay between 1 and 3 seconds.
// Implement a third function, racePromises, that races the execution of the two functions. The Promise should resolve with the message of the function that resolves first.


function racePromise1() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve("racePromise1 is successfull");    
        }, Math.random()*3000);
        
    })
}

function racePromise2() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve("racePromise2 is successfull");    
        }, Math.random()*3000);
        
    })
}

function racePromises() {
    return Promise.race([racePromise1(),racePromise2()])
}

racePromises().then((result)=>{
     console.log(result);
})