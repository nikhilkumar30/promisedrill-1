// Problem4:
// Create a function named parallelLimit.
// This function should take an array of Promises and a limit parameter (number of Promises that can run in parallel).
// Execute the Promises in parallel, but ensure that no more than the specified limit are running simultaneously.
// The function should resolve with an array containing the results of all the Promises.


const promise1 = new Promise((resolve) => {
  resolve("promise1 is successful :) ");
});

const promise2 = new Promise((resolve) => {
  resolve("promise2 is successful :) ");
});

const promise3 = new Promise((resolve) => {
  resolve("promise3 is successful :) ");
});
const promise4 = new Promise((resolve) => {
  resolve("promise4 is successful :) ");
});

const promise5 = new Promise((resolve) => {
  resolve("promise5 is successful :) ");
});

const promise6 = new Promise((resolve) => {
  resolve("promise6 is successful :) ");
});

function parallelLimit(arrayOfPromises, limit) {
  return new Promise((resolve, reject) => {
    const result = [];

    let currentIndex = 0;
    function forNextSet() {
      const currentSet = arrayOfPromises.slice(
        currentIndex,
        currentIndex + limit
      );

      Promise.allSettled(currentSet)
        .then((result) => {
          result.push(...result);
          if (currentIndex < arrayOfPromises.length) {
            forNextSet();
          } else {
            resolve(result);
          }
        })
        .catch(reject);

      currentIndex += limit;
    }
    forNextSet();
  });
}
const arrayPromises = [
  promise1,
  promise2,
  promise3,
  promise4,
  promise5,
  promise6
];

parallelLimit(arrayPromises, 3)
  .then((results) => {
    console.log("Results:", results);
  })
  .catch((error) => {
    console.error("Error occurred:", error);
  });
